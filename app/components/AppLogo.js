import React from "react";
import { View, Image, Text, StyleSheet } from "react-native";

const AppLogo = () => {
  return (
    <View style={styles.logoContainer}>
      <Image
        style={styles.logo}
        resizeMode="contain"
        source={require("../../assets/icon.png")}
      />
      <Text style={styles.logoText}>SDTC iRestaurant App</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  logoContainer: {
    alignItems: "center",
    marginBottom: 24,
  },
  logo: {
    height: 150,
    width: 150,
  },
  logoText: {
    fontSize: 24,
  },
});

export default AppLogo;
