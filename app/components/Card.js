import React from "react";
import { View, Text, StyleSheet, TouchableWithoutFeedback } from "react-native";

import colors from "../config/colors";

const Card = (props) => {
  const { title, subTitle, imageUrl, onPress, thumbnailUrl } = props;

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.card}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subTitle}>{subTitle}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    backgroundColor: colors.dark,
    paddingHorizontal: 40,
    paddingVertical: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "baseline",
    marginHorizontal: 16,
    marginVertical: 8,
  },
  subTitle: {
    color: colors.white,
    fontWeight: "bold",
    fontSize: 22,
  },
  title: {
    fontWeight: "bold",
    fontSize: 24,
    color: colors.white,
  },
});

export default Card;
