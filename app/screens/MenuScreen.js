import React, { useState } from "react";
import { useEffect } from "react";
import { SafeAreaView, Text, StyleSheet, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import AppLogo from "../components/AppLogo";
import Card from "../components/Card";
import colors from "../config/colors";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

const mockupMenu = [
  {
    id: 1,
    title: "ผัดกระเพราะหมูสับราดข้าว",
    price: 80,
  },
];

const MenuScreen = ({ navigation }) => {
  const [menus, setMenus] = useState([]);

  useEffect(() => {
    setMenus(mockupMenu);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <AppLogo />

      <View style={styles.titleContainer}>
        <Text style={styles.text}>รายการเมนู</Text>
        <TouchableWithoutFeedback onPress={() => navigation.navigate("Add")}>
          <Ionicons style={styles.addIcon} name="md-add-circle" />
        </TouchableWithoutFeedback>
      </View>

      {menus.map((m) => (
        <Card key={m.id} title={m.title} subTitle={m.price} />
      ))}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: "white",
  },
  addIcon: {
    fontSize: 50,
    color: colors.secondary,
  },
  titleContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 16,
    marginHorizontal: 16,
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default MenuScreen;
