import React, { useState } from "react";
import { Text, View } from "react-native";
import AppLogo from "../components/AppLogo";
import AppButton from "../components/Button";

const TableCommandScreen = ({ route, navigation }) => {
  const [openTable, setOpenTable] = useState(false);

  const opentableHandler = () => {
    setOpenTable(!openTable);
  };

  return (
    <View>
      <AppLogo />
      <Text>You Selected table {route.params}</Text>
      <AppButton
        title={openTable ? "ยกเลิกการเปิดโต๊ะ" : "เปิดโต๊ะ"}
        onPress={() => opentableHandler()}
      />
      <AppButton
        title="สั่งอาหาร"
        disabled={!openTable}
        onPress={() => navigation.navigate("Order")}
      />
      <AppButton title="สรุปยอดชำระเงิน" disabled={!openTable} />
    </View>
  );
};

export default TableCommandScreen;
