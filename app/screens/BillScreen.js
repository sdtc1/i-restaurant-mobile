import React from "react";
import { SafeAreaView, Text, StyleSheet } from "react-native";

const BillScreen = () => (
  <SafeAreaView style={styles.container}>
    <Text style={styles.text}>Bill screen...</Text>
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 24,
  },
});

export default BillScreen;
