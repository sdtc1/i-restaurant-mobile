import React, { useState } from "react";
import { SafeAreaView, Text, StyleSheet, View } from "react-native";
import colors from "../config/colors";

import AppLogo from "../components/AppLogo";
import AppTextInput from "../components/TextInput";

const MenuAddScreen = () => {
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);

  return (
    <SafeAreaView style={styles.container}>
      <AppLogo />

      {/* <Text style={{ alignSelf: "center", fontSize: 30 }}>เพิ่มเมนูใหม่</Text> */}

      <View style={styles.formContainer}>
        <AppTextInput
          icon="restaurant-menu"
          keyboardType="default"
          onChangeText={(text) => setName(text)}
          placeholder="ชื่อเมนู"
        />

        <AppTextInput
          icon="attach-money"
          keyboardType="numeric"
          onChangeText={(text) => setPrice(text)}
          placeholder="ราคา"
        />
      </View>

      <Text style={{ alignSelf: "center" }}>บันทึก</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: "white",
  },
  addIcon: {
    fontSize: 50,
    color: colors.secondary,
  },
  titleContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 16,
    marginHorizontal: 16,
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
  },
  formContainer: {
    padding: 24,
  },
});

export default MenuAddScreen;
