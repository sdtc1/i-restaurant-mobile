import React, { useState } from "react";
import { useEffect } from "react";
import { SafeAreaView, Text, StyleSheet, View, Image } from "react-native";

import AppLogo from "../components/AppLogo";
import Card from "../components/Card";

const mockData = [
  {
    id: 1,
    title: "1",
    subTitle: "ว่าง",
    imageUrl: "N/A",
    onPress: () => console.log("Hi Press 1"),
    thumbnailUrl: "N/A",
  },
  {
    id: 2,
    title: "2",
    subTitle: "ว่าง",
    imageUrl: "N/A",
    onPress: () => console.log("Hi Press 2"),
    thumbnailUrl: "N/A",
  },
];

const TableListScreen = ({ navigation }) => {
  const [tables, setTables] = useState([]);

  useEffect(() => {
    setTables(mockData);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <AppLogo />

      {tables.map((m) => {
        return (
          <Card
            key={m.id}
            title={m.title}
            subTitle={m.subTitle}
            onPress={() => navigation.navigate("Command", m.id)}
          />
        );
      })}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: "white",
  },
});

export default TableListScreen;
