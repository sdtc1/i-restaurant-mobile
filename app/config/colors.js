export default {
    primary: "#434B56",
    secondary: "#ACD263",
    black: "#000",
    white: "#fff",
    medium: "#6e6969",
    light: "#f8f4f4",
    dark: "#434B56",
    danger: "#ff5252",
    disabled: "#DDDDDD"
};
