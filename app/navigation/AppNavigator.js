import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialIcons, FontAwesome5 } from "@expo/vector-icons";

import MenuScreen from "../screens/MenuScreen";
import TableScreen from "../screens/TableListScreen";
import BillScreen from "../screens/BillScreen";
import MenuNavigator from "./MenuNavigator";
import TableNavigator from "./TableNavigator";

const Tab = createBottomTabNavigator();

const AppNavigator = () => (
  <Tab.Navigator>
    <Tab.Screen
      name="เมนู"
      component={MenuNavigator}
      options={{
        tabBarIcon: ({ color, size }) => (
          <MaterialIcons name="menu-book" color={color} size={size} />
        ),
      }}
    />
    <Tab.Screen
      name="โต๊ะ"
      component={TableNavigator}
      options={{
        tabBarIcon: ({ color, size }) => (
          <FontAwesome5 name="tablets" color={color} size={size} />
        ),
      }}
    />
    <Tab.Screen
      name="ชำระเงิน"
      component={BillScreen}
      options={{
        tabBarIcon: ({ color, size }) => (
          <FontAwesome5 name="money-bill-wave" color={color} size={size} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default AppNavigator;
