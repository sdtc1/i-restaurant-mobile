import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import MenuAddScreen from "../screens/MenuAddScreen";
import MenuScreen from "../screens/MenuScreen";

const Stack = createStackNavigator();

const MenuNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Home"
      component={MenuScreen}
      options={{ headerShown: false, headerTitle: "เมนู" }}
    />
    <Stack.Screen
      name="Add"
      component={MenuAddScreen}
      options={{
        headerTitle: "เพิ่มเมนูใหม่",
      }}
    />
  </Stack.Navigator>
);

export default MenuNavigator;
