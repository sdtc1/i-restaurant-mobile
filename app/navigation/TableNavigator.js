import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import TableCommandScreen from "../screens/TableCommandScreen";
import TableListScreen from "../screens/TableListScreen";
import TableOrderScreen from "../screens/TableOrderScreen";
import TableOrderMenuSelectionScreen from "../screens/TableOrderMenuSelectionScreen";

const Stack = createStackNavigator();

const TableNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Home"
      component={TableListScreen}
      options={{ headerTitle: "โต๊ะทั้งหมด", headerShown: false }}
    />
    <Stack.Screen
      name="Command"
      component={TableCommandScreen}
      options={{ headerTitle: "คำสั่ง" }}
    />
    <Stack.Screen
      name="Order"
      component={TableOrderScreen}
      options={{ headerTitle: "สั่งอาหาร" }}
    />
    <Stack.Screen
      name="MenuSelection"
      component={TableOrderMenuSelectionScreen}
      options={{ headerTitle: "สั่งอาหาร" }}
    />
  </Stack.Navigator>
);

export default TableNavigator;
